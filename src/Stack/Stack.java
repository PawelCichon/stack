package Stack;

public interface Stack<T> {

	T push(T item);
	
	T pop();
	
	T peek();
	
	int size();
	
	boolean isEmpty();
	
	boolean isFull();
	
	void grow(int value);
	
}
