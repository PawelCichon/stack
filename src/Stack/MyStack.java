package Stack;

/**
 * MyStack implements Stack interface. MyStack Implements seven operations.
 * Metod's push, peek and pop represents a LIFO(last in first out). �Push� Pushes next object to top stack,
 * �pop� delete top from stack, �peek� take object on top stack. �isEmpty� and �isFull� method test Stack. 
 * �size� returns the number of elements in this stack. "grow" increase max size Stack.
 * 
 * @author Pawe� Cicho�.
 * @param <T> This is the type parameter.
 */
public class MyStack<T> implements Stack<T> {
	
	/**
	 * Actual top object.
	 */
	private T top;
	
	/**
	 * Max size stack, and actual size Stack.
	 */
	private int maxSize,size;
	
	/**
	 * Class Element containers previous instant object.
	 * @param <T> This is the type parameter.
	 */
	private Element<T> element;
	private class Element<T> {
		public Element prev;
		
		public T t;
		
		public Element(T t, Element prev) {
			this.prev = prev;
			this.t= t;
		}
	}
	
	/**
	 * Constructor create an empty Stack.
	 * @param maxSize Max number Object in Stack.
	 */
	public MyStack(int maxSize) {
		top = null;
		this.maxSize = maxSize;
	}

	/**
	 * Add new object on top Stack. 
	 * @param item New object to push on stack.
	 * @return object on top Stack.
	 */
	@Override
	public T push(T item) {
		if (!isFull()){
			element = new Element(item, element);
			size++;
			return top = element.t;
		}else {
			throw new IndexOutOfBoundsException("Stack is full, stack max size is " + size);
		}
	}
	
	/**
	 * Tests if Stack is full.
	 * @return true if this stack is full object; false otherwise.
	 */
	@Override
	public boolean isFull(){
        return size >= maxSize;
    }
	
	/**
	 * Removes the object at the top of this stack, end add to top previous Object.
	 * @return Actual top object
	 */
	@Override
	public T pop() {
		if(isEmpty()){
			return top = null;
		}else if(size == 1){
			size--;
			element = element.prev;
			top = null;
			return top;
		}else{
			size--;
			element = element.prev;
			top = element.t;
			return top;
		}
	}
	
	/** Tests if this stack is empty.
	 * @return true if this stack contains no items; false otherwise.
	 */
	@Override
	public boolean isEmpty(){
		return top == null;
	}
	
	/** Take Object from top Stack.
	 * @return top Object from Stack.
	 */
	@Override
	public T peek(){
		return top;
	}
	
	/**
	 * Returns the number of elements in this stack.
	 * @return the number of elements in this stack.
	 */
	@Override
	public int size(){
		return size;
	}
	
	/**
	 * Increase max size Stack.
	 * @param value increase max size Stack.
	 */
	@Override
	public void grow(int value) {
		maxSize = maxSize + value;
	}
	
}
