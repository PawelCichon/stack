package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Stack.MyStack;
import Stack.Stack;

public class TestMyStack {
	
	@Test
	public void createEmptyStack() {
		Assert.assertNull(stack.peek());
		Assert.assertEquals(stack.isEmpty(), true);
	}
	
	@Test 
	public void push4Object(){
		pushObjectsInOrder(OBJECT_1, OBJECT_2, OBJECT_3);
		Assert.assertEquals(stack.peek(), OBJECT_3);
		Assert.assertEquals(stack.size(), 3);
	}
	
	@Test 
	public void pushMaxObject(){
		pushObjectsInOrder(OBJECT_1, OBJECT_2, OBJECT_3, OBJECT_4);
		Assert.assertEquals(stack.isFull(), true);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void createFullStackAndMore() {
		pushObjectsInOrder(OBJECT_1, OBJECT_2, OBJECT_3, OBJECT_4, OBJECT_5);
	}
	
	@Test 
	public void pop1Object(){
		pushObjectsInOrder(OBJECT_1, OBJECT_2);
		Assert.assertEquals(stack.pop(), OBJECT_1 );
		Assert.assertEquals(stack.size(), 1);
	}
	
	@Test 
	public void popForNull(){
		pushObjectsInOrder(OBJECT_1, OBJECT_2);
		stack.pop();
		Assert.assertNull(stack.pop());
		Assert.assertEquals(stack.isEmpty(), true);
	}
	
	@Test
	public void growUp(){
		pushObjectsInOrder(OBJECT_1, OBJECT_2, OBJECT_3, OBJECT_4);
		stack.grow(1);
		stack.push(OBJECT_5);
		Assert.assertEquals(stack.isFull(), true);
	}
	
	
private Stack<Object> stack;
	
	@Before
	public void setUp(){
		stack = new MyStack<Object>(4);
	}
	
	private void pushObjectsInOrder(Object... objects) {
		for (Object object : objects) {
			stack.push(object);
		}
	}
	
	private static final Object OBJECT_1 = new Object();
    private static final Object OBJECT_2 = new Object();
    private static final Object OBJECT_3 = new Object();
    private static final Object OBJECT_4 = new Object();
    private static final Object OBJECT_5 = new Object();
}
